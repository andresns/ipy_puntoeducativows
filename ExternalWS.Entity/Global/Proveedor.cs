﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Entity.Global
{
    public class Proveedor : IPersistentBase
    {

        public override bool IsEqual(IPersistentBase p)
        {
            throw new NotImplementedException();
        }


        #region propierties
        [JsonProperty("runProveedor")]
        public string runProveedor
        {
            get { return _runProveedor; }
            set
            {
                _runProveedor = value;
            }
        }
        [JsonProperty("nombreProveedor")]
        public string nombreProveedor
        {
            get { return _nombreProveedor; }
            set
            {
                _nombreProveedor = value;
            }
        }
        [JsonProperty("direcionProveedor")]
        public string direcionProveedor
        {
            get { return _direcionProveedor; }
            set
            {
                _direcionProveedor = value;
            }
        }
        [JsonProperty("fonoProveedor")]
        public int fonoProveedor
        {
            get { return _fonoProveedor; }
            set
            {
                _fonoProveedor = value;
            }
        }
        [JsonProperty("emailProveedor")]
        public string emailProveedor
        {
            get { return _emailProveedor; }
            set
            {
                _emailProveedor = value;
            }
        }
        #endregion


        #region members
        protected string _runProveedor;
        protected string _nombreProveedor;
        protected string _direcionProveedor;
        protected int _fonoProveedor;
        protected string _emailProveedor;
        #endregion
    }

    public class FilterProveedor : IPersistentBase
    {

        public override bool IsEqual(IPersistentBase p)
        {
            throw new NotImplementedException();
        }


        #region propierties
        [JsonProperty("runProveedor")]
        public string runProveedor
        {
            get { return _runProveedor; }
            set
            {
                _runProveedor = value;
            }
        }

        [JsonProperty("nombreProveedor")]
        public string nombreProveedor
        {
            get { return _nombreProveedor; }
            set
            {
                _nombreProveedor = value;
            }
        }

        [JsonProperty("direcionProveedor")]
        public string direcionProveedor
        {
            get { return _direcionProveedor; }
            set
            {
                _direcionProveedor = value;
            }
        }

        [JsonProperty("fonoProveedor")]
        public int fonoProveedor
        {
            get { return _fonoProveedor; }
            set
            {
                _fonoProveedor = value;
            }
        }

        [JsonProperty("emailProveedor")]
        public string emailProveedor
        {
            get { return _emailProveedor; }
            set
            {
                _emailProveedor = value;
            }
        }

        #endregion


        #region members
        // Generated code:
        protected string _runProveedor;
        // Generated code:
        protected string _nombreProveedor;
        // Generated code:
        protected string _direcionProveedor;
        // Generated code:
        protected int _fonoProveedor;
        // Generated code:
        protected string _emailProveedor;
        #endregion
    }
}
