﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Entity.Global
{
    public class CabeceraOrdenCompra : IPersistentBase
    {

        public override bool IsEqual(IPersistentBase p)
        {
            throw new NotImplementedException();
        }


        #region propierties
        [JsonProperty("codOrdenCompra")]
        public string codOrdenCompra
        {
            get { return _codOrdenCompra; }
            set
            {
                _codOrdenCompra = value;
            }
        }
        [JsonProperty("runCliente")]
        public string runCliente
        {
            get { return _runCliente; }
            set
            {
                _runCliente = value;
            }
        }
        [JsonProperty("nombreCliente")]
        public string nombreCliente
        {
            get { return _nombreCliente; }
            set
            {
                _nombreCliente = value;
            }
        }
        [JsonProperty("direcionCliente")]
        public string direcionCliente
        {
            get { return _direcionCliente; }
            set
            {
                _direcionCliente = value;
            }
        }
        [JsonProperty("fonoCliente")]
        public int fonoCliente
        {
            get { return _fonoCliente; }
            set
            {
                _fonoCliente = value;
            }
        }
        [JsonProperty("emailCliente")]
        public string emailCliente
        {
            get { return _emailCliente; }
            set
            {
                _emailCliente = value;
            }
        }
        [JsonProperty("fecha")]
        public DateTime fecha
        {
            get { return _fecha; }
            set
            {
                _fecha = value;
            }
        }
        #endregion


        #region members
        protected string _codOrdenCompra;
        protected string _runCliente;
        protected string _nombreCliente;
        protected string _direcionCliente;
        protected int _fonoCliente;
        protected string _emailCliente;
        protected DateTime _fecha;
        #endregion
    }

    public class FilterCabeceraOrdenCompra : IPersistentBase
    {

        public override bool IsEqual(IPersistentBase p)
        {
            throw new NotImplementedException();
        }


        #region propierties
        [JsonProperty("codOrdenCompra")]
        public string codOrdenCompra
        {
            get { return _codOrdenCompra; }
            set
            {
                _codOrdenCompra = value;
            }
        }

        [JsonProperty("runCliente")]
        public string runCliente
        {
            get { return _runCliente; }
            set
            {
                _runCliente = value;
            }
        }

        [JsonProperty("nombreCliente")]
        public string nombreCliente
        {
            get { return _nombreCliente; }
            set
            {
                _nombreCliente = value;
            }
        }

        [JsonProperty("direcionCliente")]
        public string direcionCliente
        {
            get { return _direcionCliente; }
            set
            {
                _direcionCliente = value;
            }
        }

        [JsonProperty("fonoCliente")]
        public int fonoCliente
        {
            get { return _fonoCliente; }
            set
            {
                _fonoCliente = value;
            }
        }

        [JsonProperty("emailCliente")]
        public string emailCliente
        {
            get { return _emailCliente; }
            set
            {
                _emailCliente = value;
            }
        }

        [JsonProperty("fecha")]
        public DateTime fecha
        {
            get { return _fecha; }
            set
            {
                _fecha = value;
            }
        }

        #endregion


        #region members
        // Generated code:
        protected string _codOrdenCompra;
        // Generated code:
        protected string _runCliente;
        // Generated code:
        protected string _nombreCliente;
        // Generated code:
        protected string _direcionCliente;
        // Generated code:
        protected int _fonoCliente;
        // Generated code:
        protected string _emailCliente;
        // Generated code:
        protected DateTime _fecha;
        #endregion
    }
}
