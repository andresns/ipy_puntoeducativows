﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Entity.Global
{
    public class RequestParameterProducto
    {
        public FilterProducto FilterProducto { get; set; }
        public Autentication Autentication { get; set; }
    }
}
