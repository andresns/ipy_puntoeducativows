﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Entity.Global
{
    public class DetalleOrdenCompra : IPersistentBase
    {

        public override bool IsEqual(IPersistentBase p)
        {
            throw new NotImplementedException();
        }


        #region propierties
        [JsonProperty("cantidad")]
        public int cantidad
        {
            get { return _cantidad; }
            set
            {
                _cantidad = value;
            }
        }
        [JsonProperty("precioUnitario")]
        public int precioUnitario
        {
            get { return _precioUnitario; }
            set
            {
                _precioUnitario = value;
            }
        }
        [JsonProperty("idProducto")]
        public int idProducto
        {
            get { return _idProducto; }
            set
            {
                _idProducto = value;
            }
        }
        [JsonProperty("codOrdenCompra")]
        public string codOrdenCompra
        {
            get { return _codOrdenCompra; }
            set
            {
                _codOrdenCompra = value;
            }
        }
        #endregion


        #region members
        protected int _cantidad;
        protected int _precioUnitario;
        protected int _idProducto;
        protected string _codOrdenCompra;
        #endregion
    }

    public class FilterDetalleOrdenCompra : IPersistentBase
    {

        public override bool IsEqual(IPersistentBase p)
        {
            throw new NotImplementedException();
        }


        #region propierties
        [JsonProperty("cantidad")]
        public int cantidad
        {
            get { return _cantidad; }
            set
            {
                _cantidad = value;
            }
        }

        [JsonProperty("precioUnitario")]
        public int precioUnitario
        {
            get { return _precioUnitario; }
            set
            {
                _precioUnitario = value;
            }
        }

        [JsonProperty("idProducto")]
        public int idProducto
        {
            get { return _idProducto; }
            set
            {
                _idProducto = value;
            }
        }

        [JsonProperty("codOrdenCompra")]
        public string codOrdenCompra
        {
            get { return _codOrdenCompra; }
            set
            {
                _codOrdenCompra = value;
            }
        }

        #endregion


        #region members
        // Generated code:
        protected int _cantidad;
        // Generated code:
        protected int _precioUnitario;
        // Generated code:
        protected int _idProducto;
        // Generated code:
        protected string _codOrdenCompra;
        #endregion
    }
}
