﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Entity.Global
{
    public class ResponseParameterProducto
    {
        public List<Producto> Productos { get; set; }
        public ResponseTransaction ResponseTransaction { get; set; }
    }
}
