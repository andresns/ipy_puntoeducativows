﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Entity.Global
{
    public class Producto : IPersistentBase
    {

        public override bool IsEqual(IPersistentBase p)
        {
            throw new NotImplementedException();
        }


        #region propierties
        [JsonProperty("idProducto")]
        public int idProducto
        {
            get { return _idProducto; }
            set
            {
                _idProducto = value;
            }
        }
        [JsonProperty("descripcion")]
        public string descripcion
        {
            get { return _descripcion; }
            set
            {
                _descripcion = value;
            }
        }
        #endregion


        #region members
        protected int _idProducto;
        protected string _descripcion;
        #endregion
    }

    public class FilterProducto : IPersistentBase
    {

        public override bool IsEqual(IPersistentBase p)
        {
            throw new NotImplementedException();
        }


        #region propierties
        [JsonProperty("idProducto")]
        public int idProducto
        {
            get { return _idProducto; }
            set
            {
                _idProducto = value;
            }
        }

        [JsonProperty("descripcion")]
        public string descripcion
        {
            get { return _descripcion; }
            set
            {
                _descripcion = value;
            }
        }

        #endregion


        #region members
        // Generated code:
        protected int _idProducto;
        // Generated code:
        protected string _descripcion;
        #endregion
    }
}
