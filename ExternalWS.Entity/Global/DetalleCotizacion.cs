﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Entity.Global
{
    public class DetalleCotizacion : IPersistentBase
    {

        public override bool IsEqual(IPersistentBase p)
        {
            throw new NotImplementedException();
        }


        #region propierties
        [JsonProperty("cantidad")]
        public int cantidad
        {
            get { return _cantidad; }
            set
            {
                _cantidad = value;
            }
        }
        [JsonProperty("precioUnitario")]
        public int precioUnitario
        {
            get { return _precioUnitario; }
            set
            {
                _precioUnitario = value;
            }
        }
        [JsonProperty("idProducto")]
        public int idProducto
        {
            get { return _idProducto; }
            set
            {
                _idProducto = value;
            }
        }
        [JsonProperty("codCotizacion")]
        public string codCotizacion
        {
            get { return _codCotizacion; }
            set
            {
                _codCotizacion = value;
            }
        }
        #endregion


        #region members
        protected int _cantidad;
        protected int _precioUnitario;
        protected int _idProducto;
        protected string _codCotizacion;
        #endregion
    }

    public class FilterDetalleCotizacion : IPersistentBase
    {

        public override bool IsEqual(IPersistentBase p)
        {
            throw new NotImplementedException();
        }


        #region propierties
        [JsonProperty("cantidad")]
        public int cantidad
        {
            get { return _cantidad; }
            set
            {
                _cantidad = value;
            }
        }

        [JsonProperty("precioUnitario")]
        public int precioUnitario
        {
            get { return _precioUnitario; }
            set
            {
                _precioUnitario = value;
            }
        }

        [JsonProperty("idProducto")]
        public int idProducto
        {
            get { return _idProducto; }
            set
            {
                _idProducto = value;
            }
        }

        [JsonProperty("codCotizacion")]
        public string codCotizacion
        {
            get { return _codCotizacion; }
            set
            {
                _codCotizacion = value;
            }
        }

        #endregion


        #region members
        // Generated code:
        protected int _cantidad;
        // Generated code:
        protected int _precioUnitario;
        // Generated code:
        protected int _idProducto;
        // Generated code:
        protected string _codCotizacion;
        #endregion
    }
}
