﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Entity.Global
{
    public class CabeceraCotizacion : IPersistentBase
    {

        public override bool IsEqual(IPersistentBase p)
        {
            throw new NotImplementedException();
        }


        #region propierties
        [JsonProperty("codCotizacion")]
        public string codCotizacion
        {
            get { return _codCotizacion; }
            set
            {
                _codCotizacion = value;
            }
        }
        [JsonProperty("fecha")]
        public DateTime fecha
        {
            get { return _fecha; }
            set
            {
                _fecha = value;
            }
        }
        [JsonProperty("runProveedor")]
        public string runProveedor
        {
            get { return _runProveedor; }
            set
            {
                _runProveedor = value;
            }
        }
        #endregion


        #region members
        protected string _codCotizacion;
        protected DateTime _fecha;
        protected string _runProveedor;
        #endregion
    }

    public class FilterCabeceraCotizacion : IPersistentBase
    {

        public override bool IsEqual(IPersistentBase p)
        {
            throw new NotImplementedException();
        }


        #region propierties
        [JsonProperty("codCotizacion")]
        public string codCotizacion
        {
            get { return _codCotizacion; }
            set
            {
                _codCotizacion = value;
            }
        }

        [JsonProperty("fecha")]
        public DateTime fecha
        {
            get { return _fecha; }
            set
            {
                _fecha = value;
            }
        }

        [JsonProperty("runProveedor")]
        public string runProveedor
        {
            get { return _runProveedor; }
            set
            {
                _runProveedor = value;
            }
        }

        #endregion


        #region members
        // Generated code:
        protected string _codCotizacion;
        // Generated code:
        protected DateTime _fecha;
        // Generated code:
        protected string _runProveedor;
        #endregion
    }
}
