﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Entity.Global
{
    public class ResponseTransaction
    {
        public int Code { get; set; }
        public String Description { get; set; }
    }
}
