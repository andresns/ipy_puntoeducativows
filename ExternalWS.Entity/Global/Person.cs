﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Entity.Global
{
    public class Person : IPersistentBase
    {

        public override bool IsEqual(IPersistentBase p)
        {
            throw new NotImplementedException();
        }


        #region propierties
        [JsonProperty("Names")]
        public string Names
        {
            get { return _names; }
            set
            {
                _names = value;
            }
        }
        [JsonProperty("FathersFamilyName")]
        public string FathersFamilyName
        {
            get { return _fathersFamilyName; }
            set
            {
                _fathersFamilyName = value;
            }
        }
        [JsonProperty("MothersFamilyName")]
        public string MothersFamilyName
        {
            get { return _mothersFamilyName; }
            set
            {
                _mothersFamilyName = value;
            }
        }
        [JsonProperty("Email")]
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
            }
        }
        [JsonProperty("BirthDate")]
        public DateTime BirthDate
        {
            get { return _birthDate; }
            set
            {
                _birthDate = value;
            }
        }
        [JsonProperty("GlobalId")]
        public string GlobalId
        {
            get { return _globalId; }
            set
            {
                _globalId = value;
            }
        }
        #endregion


        #region members
        protected string _names;
        protected string _fathersFamilyName;
        protected string _mothersFamilyName;
        protected string _email;
        protected DateTime _birthDate;
        protected string _globalId;
        #endregion
    }

    public class FilterPerson : IPersistentBase
    {

        public override bool IsEqual(IPersistentBase p)
        {
            throw new NotImplementedException();
        }


        #region propierties
        [JsonProperty("Names")]
        public string Names
        {
            get { return _names; }
            set
            {
                _names = value;
            }
        }

        [JsonProperty("FathersFamilyName")]
        public string FathersFamilyName
        {
            get { return _fathersFamilyName; }
            set
            {
                _fathersFamilyName = value;
            }
        }

        [JsonProperty("MothersFamilyName")]
        public string MothersFamilyName
        {
            get { return _mothersFamilyName; }
            set
            {
                _mothersFamilyName = value;
            }
        }

        [JsonProperty("Email")]
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
            }
        }

        [JsonProperty("BirthDate")]
        public DateTime BirthDate
        {
            get { return _birthDate; }
            set
            {
                _birthDate = value;
            }
        }

        [JsonProperty("GlobalId")]
        public string GlobalId
        {
            get { return _globalId; }
            set
            {
                _globalId = value;
            }
        }

        #endregion


        #region members
        // Generated code:
        protected string _names;
        // Generated code:
        protected string _fathersFamilyName;
        // Generated code:
        protected string _mothersFamilyName;
        // Generated code:
        protected string _email;
        // Generated code:
        protected DateTime _birthDate;
        // Generated code:
        protected string _globalId;
        #endregion
    }
}