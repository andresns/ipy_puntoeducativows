﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Utils
{
    public class Encryption
    {
        public static string GetEncryptedTicket(string healthCareCenter, string login, DateTime expiration, int idPT, string guid, byte[] TDESKey)
        {
            string stringToBeEncrypted = string.Format("{0}/{1}/{2}/{3}/{4}", healthCareCenter, login, expiration.Ticks.ToString(), guid, idPT);
            string encryptedString = EncryptString(stringToBeEncrypted, TDESKey);
            return encryptedString;

        }

        public static string GetHealthCareCenterInTicket(string ticket, byte[] TDESKey)
        {
            try
            {
                string decipheredString = DecryptString(ticket, TDESKey);
                string[] parts = decipheredString.Split(new char[] { '/' }, 5);
                return parts[0];
            }
            catch
            {

            }
            return null;
        }

        public static bool TicketIsValid(string encryptedTicket, byte[] TDESKey, out string healthCareCenter, out string login, out DateTime expiration, out string guid, out int idPT)
        {
  
            try
            {
                string cadenaDescifrada = DecryptString(encryptedTicket, TDESKey);
                string[] parts = cadenaDescifrada.Split(new char[] { '/' }, 5);
                expiration = new DateTime(Int64.Parse(parts[2]));
                healthCareCenter = parts[0];
                login = parts[1];
                guid = parts[3];

                Int32.TryParse(parts[4], out idPT);
                return (expiration > DateTime.Now);
            }
            catch
            {
            }
            guid = null;
            healthCareCenter = null;
            login = null;
            expiration = DateTime.MinValue;
            idPT = 0;
            return false;
        }

        public static byte[] GetTDESKey(string frasePassword)
        {
     
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

            // Step 1. We hash the passphrase using MD5
            // We use the MD5 hash generator as the result is a 128 bit byte array
            // which is a valid length for the TripleDES encoder we use below

            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(frasePassword));
            return TDESKey;

        }

        static string EncryptString(string Message, byte[] TDESKey)
        {

            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

            // Step 2. Create a new TripleDESCryptoServiceProvider object
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

            // Step 3. Setup the encoder
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;

            // Step 4. Convert the input string to a byte[]
            byte[] DataToEncrypt = UTF8.GetBytes(Message);

            // Step 5. Attempt to encrypt the string
            try

            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
            }

            finally

            {
                // Clear the TripleDes and Hashprovider services of any sensitive information
                TDESAlgorithm.Clear();

            }

            // Step 6. Return the encrypted string as a base64 encoded string
            return Convert.ToBase64String(Results);
        }

        static string DecryptString(string Message, byte[] TDESKey)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            // Step 2. Create a new TripleDESCryptoServiceProvider object
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

            // Step 3. Setup the decoder
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;

            // Step 4. Convert the input string to a byte[]
            byte[] DataToDecrypt = Convert.FromBase64String(Message);

            // Step 5. Attempt to decrypt the string
            try

            {
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
            }

            finally

            {
                // Clear the TripleDes and Hashprovider services of any sensitive information
                TDESAlgorithm.Clear();
            }

            // Step 6. Return the decrypted string in UTF8 format
            return UTF8.GetString(Results);
        }
        public Encryption()
        {

        }
    }
}
