﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Utils
{
    public class InvalidUsernamePassException : Exception
    {

        public InvalidUsernamePassException() : base() { }
        public InvalidUsernamePassException(string message) : base(message) { }
        public InvalidUsernamePassException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client. 
        protected InvalidUsernamePassException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
        { }
    }
}
