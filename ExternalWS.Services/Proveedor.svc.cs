﻿using ExternalWS.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ExternalWS.Services
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Proveedor" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Proveedor.svc o Proveedor.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Proveedor : IProveedor
    {
        public List<ExternalWS.DataContracts.Proveedor> GetProveedor(FilterProveedor filterProveedor)
        {
            List<ExternalWS.DataContracts.Proveedor> proveedores = new List<DataContracts.Proveedor>();
            ExternalWS.Business.Global.Proveedor boProveedor = new Business.Global.Proveedor();
            ExternalWS.Entity.Global.FilterProveedor fProveedor = new Entity.Global.FilterProveedor()
            {
                runProveedor = filterProveedor.RunProveedor,
                nombreProveedor = filterProveedor.NombreProveedor,
                direcionProveedor = filterProveedor.DireccionProveedor,
                fonoProveedor = filterProveedor.FonoProveedor,
                emailProveedor = filterProveedor.EmailProveedor
            };
            List<ExternalWS.Entity.Global.Proveedor> proveedoresEntity = boProveedor.GetProveedor(fProveedor);

            if (proveedoresEntity != null && proveedoresEntity.Count > 0)
            {
                foreach (ExternalWS.Entity.Global.Proveedor p in proveedoresEntity)
                {
                    ExternalWS.DataContracts.Proveedor proveedor = new DataContracts.Proveedor()
                    {
                        RunProveedor = p.runProveedor,
                        NombreProveedor = p.nombreProveedor,
                        DireccionProveedor = p.direcionProveedor,
                        FonoProveedor = p.fonoProveedor,
                        EmailProveedor = p.emailProveedor
                    };
                    proveedores.Add(proveedor);
                }
            }
            return proveedores;
        }
    }
}
