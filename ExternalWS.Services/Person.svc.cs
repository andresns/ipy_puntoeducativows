﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ExternalWS.DataContracts;

namespace ExternalWS.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Person" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Person.svc or Person.svc.cs at the Solution Explorer and start debugging.
    public class Person : IPerson
    {
        public List<ExternalWS.DataContracts.Person> GetPerson(FilterPerson filterPerson)
        {
            List<ExternalWS.DataContracts.Person> persons = new List<DataContracts.Person>();
            ExternalWS.Business.Global.Person boPerson = new Business.Global.Person();
            ExternalWS.Entity.Global.FilterPerson fPerson = new Entity.Global.FilterPerson()
            {
                Names = filterPerson.Names,
                FathersFamilyName = filterPerson.FathersFamilyName,
                MothersFamilyName = filterPerson.MothersFamilyName,
                BirthDate = filterPerson.BirthDate,
                GlobalId = filterPerson.GlobalId
            };
            List<ExternalWS.Entity.Global.Person> personsEntity = boPerson.GetPerson(fPerson);
            
            if (personsEntity != null && personsEntity.Count > 0)
            {
                foreach (ExternalWS.Entity.Global.Person p in personsEntity)
                {
                    ExternalWS.DataContracts.Person person = new DataContracts.Person()
                    {
                        BirthDate = p.BirthDate,
                        Names = p.Names,
                        FathersFamilyName = p.FathersFamilyName,
                        MothersFamilyName = p.MothersFamilyName,
                        Email = p.Email,
                        GlobalId = p.GlobalId
                    };
                    persons.Add(person);
                }
            }
            return persons;
        }
    }
}
