﻿using ExternalWS.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ExternalWS.Services
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "DetalleCotizacion" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione DetalleCotizacion.svc o DetalleCotizacion.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class DetalleCotizacion : IDetalleCotizacion
    {
        public List<ExternalWS.DataContracts.DetalleCotizacion> GetDetalleCotizacion(FilterDetalleCotizacion filterDetalleCotizacion)
        {
            List<ExternalWS.DataContracts.DetalleCotizacion> detalleCotizaciones = new List<DataContracts.DetalleCotizacion>();
            ExternalWS.Business.Global.DetalleCotizacion boDetalleCotizacion = new Business.Global.DetalleCotizacion();
            ExternalWS.Entity.Global.FilterDetalleCotizacion fDetalleCotizacion = new Entity.Global.FilterDetalleCotizacion()
            {
                cantidad = filterDetalleCotizacion.Cantidad,
                precioUnitario = filterDetalleCotizacion.PrecioUnitario,
                idProducto = filterDetalleCotizacion.IdProducto,
                codCotizacion = filterDetalleCotizacion.CodCotizacion
            };
            List<ExternalWS.Entity.Global.DetalleCotizacion> detalleCotizacionesEntity = boDetalleCotizacion.GetDetalleCotizacion(fDetalleCotizacion);

            if (detalleCotizacionesEntity != null && detalleCotizacionesEntity.Count > 0)
            {
                foreach (ExternalWS.Entity.Global.DetalleCotizacion dc in detalleCotizacionesEntity)
                {
                    ExternalWS.DataContracts.DetalleCotizacion detalleCotizacion = new DataContracts.DetalleCotizacion()
                    {
                        Cantidad = dc.cantidad,
                        PrecioUnitario = dc.precioUnitario,
                        IdProducto = dc.idProducto,
                        CodCotizacion = dc.codCotizacion
                    };
                    detalleCotizaciones.Add(detalleCotizacion);
                }
            }
            return detalleCotizaciones;
        }
    }
}
