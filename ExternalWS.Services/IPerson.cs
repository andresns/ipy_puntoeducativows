﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ExternalWS.DataContracts;

namespace ExternalWS.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPerson" in both code and config file together.
    [ServiceContract]
    public interface IPerson
    {
        [OperationContract]
        List<ExternalWS.DataContracts.Person> GetPerson(FilterPerson filterPerson);
    }
}
