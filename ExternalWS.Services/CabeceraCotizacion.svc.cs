﻿using ExternalWS.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ExternalWS.Services
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "CabeceraCotizacion" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione CabeceraCotizacion.svc o CabeceraCotizacion.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class CabeceraCotizacion : ICabeceraCotizacion
    {
        public List<ExternalWS.DataContracts.CabeceraCotizacion> GetCabeceraCotizacion(FilterCabeceraCotizacion filterCabeceraCotizacion)
        {
            List<ExternalWS.DataContracts.CabeceraCotizacion> cabeceraCotizaciones = new List<DataContracts.CabeceraCotizacion>();
            ExternalWS.Business.Global.CabeceraCotizacion boCabeceraCotizacion = new Business.Global.CabeceraCotizacion();
            ExternalWS.Entity.Global.FilterCabeceraCotizacion fCabeceraCotizacion = new Entity.Global.FilterCabeceraCotizacion()
            {
                codCotizacion = filterCabeceraCotizacion.CodCotizacion,
                fecha = filterCabeceraCotizacion.Fecha,
                runProveedor = filterCabeceraCotizacion.RunProveedor
            };
            List<ExternalWS.Entity.Global.CabeceraCotizacion> cabeceraCotizacionesEntity = boCabeceraCotizacion.GetCabeceraCotizacion(fCabeceraCotizacion);

            if (cabeceraCotizacionesEntity != null && cabeceraCotizacionesEntity.Count > 0)
            {
                foreach (ExternalWS.Entity.Global.CabeceraCotizacion cc in cabeceraCotizacionesEntity)
                {
                    ExternalWS.DataContracts.CabeceraCotizacion cabeceraCotizacion = new DataContracts.CabeceraCotizacion()
                    {
                        CodCotizacion = cc.codCotizacion,
                        Fecha = cc.fecha,
                        RunProveedor = cc.runProveedor
                    };
                    cabeceraCotizaciones.Add(cabeceraCotizacion);
                }
            }
            return cabeceraCotizaciones;
        }
    }
}
