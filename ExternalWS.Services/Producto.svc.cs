﻿using ExternalWS.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ExternalWS.Services
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Producto" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Producto.svc o Producto.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Producto : IProducto
    {
        public ExternalWS.DataContracts.ResponseParameterProducto GetProducto(FilterProducto filterProducto)
        {
            ExternalWS.DataContracts.RequestParameterProducto requestParameterProducto = new ExternalWS.DataContracts.RequestParameterProducto();
            ExternalWS.DataContracts.ResponseParameterProducto responseParameterProducto = new ExternalWS.DataContracts.ResponseParameterProducto();

            requestParameterProducto.Autentication = new Autentication()
            {
                SystemName = filterProducto.SystemName,
                Password = filterProducto.Password
            };

            requestParameterProducto.FilterProducto = new FilterProducto()
            {
                IdProducto = filterProducto.IdProducto,
                Descripcion = filterProducto.Descripcion
            };

            responseParameterProducto.ResponseTransaction = Validate.ValidateRequest(requestParameterProducto);

            if(responseParameterProducto.ResponseTransaction.Code != 0)
            {
                return responseParameterProducto;
            }
            else
            {
                try
                {
                    List<ExternalWS.DataContracts.Producto> productos = new List<DataContracts.Producto>();
                    ExternalWS.Business.Global.Producto boProducto = new Business.Global.Producto();
                    ExternalWS.Entity.Global.FilterProducto fProducto = new Entity.Global.FilterProducto()
                    {
                        idProducto = filterProducto.IdProducto,
                        descripcion = filterProducto.Descripcion
                    };
                    List<ExternalWS.Entity.Global.Producto> productosEntity = boProducto.GetProducto(fProducto);

                    if (productosEntity != null && productosEntity.Count > 0)
                    {
                        foreach (ExternalWS.Entity.Global.Producto p in productosEntity)
                        {
                            ExternalWS.DataContracts.Producto producto = new DataContracts.Producto()
                            {
                                IdProducto = p.idProducto,
                                Descripcion = p.descripcion
                            };
                            productos.Add(producto);
                        }
                    }

                    responseParameterProducto.Productos = productos;
                    responseParameterProducto.ResponseTransaction.Code = (int)ExternalWS.Entity.WSException.Success;
                    responseParameterProducto.ResponseTransaction.Description = "Transacción Exitosa";

                    return responseParameterProducto;

                }
                catch (Exception ex)
                {
                    responseParameterProducto.ResponseTransaction.Code = (int)ExternalWS.Entity.WSException.GeneralError;
                    responseParameterProducto.ResponseTransaction.Description = string.Format("{0}-{1}", "Excepción General", ex.Message);
                    
                    return responseParameterProducto;
                }
            }
        }
    }
}
