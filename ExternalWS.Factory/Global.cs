﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Factory
{
    public class Global
    {
        const string MODEL_OBJECT = "Global";

        public static IData.IGlobal Create()
        {
            /// Look up the DAL implementation we should be using
            string path = ConfigurationManager.AppSettings[ExternalWS.Factory.General.CONFIG_PATH];
            string className = path + "." + MODEL_OBJECT;
            try
            {

                // Using the evidence given in the config file load the appropriate assembly and class
                return (IData.IGlobal)Assembly.Load(path).CreateInstance(className);
            }
            catch (Exception ex)
            {
                string exe = ex.Message;
            }
            return null;
        }
    }
}