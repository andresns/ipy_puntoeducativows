﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Threading;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace ExternalWS.IData
{
    public class DBCommandWrapper
    {
        public DbCommand Command;

        public void AddInParameter(string name, DbType type, object value)
        {
            DbParameter newParameter = Command.CreateParameter();
            newParameter.DbType = type;
            if (name[0] != '@')
                name = "@" + name;
            newParameter.ParameterName = name;
            if (value == null)
                newParameter.Value = System.DBNull.Value;
            else
                newParameter.Value = value;
            newParameter.Direction = ParameterDirection.Input;
            Command.Parameters.Add(newParameter);
        }
        public void AddInParameter(string name, DbType type)
        {
            if (name[0] != '@')
                name = "@" + name;
            AddInParameter(name, type, null);
        }

        public void SetParameterValue(string name, object value)
        {
            if (name[0] != '@')
                name = "@" + name;
            Command.Parameters[Command.Parameters.IndexOf(name)].Value = value;
        }
        public void AddOutParameter(string name, DbType type, int size)
        {
            DbParameter newParametro = Command.CreateParameter();
            newParametro.DbType = type;
            if (name[0] != '@')
                name = "@" + name;
            newParametro.ParameterName = name;
            newParametro.Size = size;
            newParametro.Direction = ParameterDirection.Output;
            Command.Parameters.Add(newParametro);
        }
        public object GetParameterValue(string name)
        {

            if (name[0] != '@')
                name = "@" + name;
            return Command.Parameters[Command.Parameters.IndexOf(name)].Value;
        }
    }
    public class TransactionalHandler
    {
        private string _dbName;

        public TransactionalHandler()
        {
            _dbName = string.Empty;
        }
        public void LoadDataSet(DBCommandWrapper command, DataSet dataSet, string tableName)
        {
            Mgr.Instance.LoadDataSet(_dbName, command.Command, dataSet, tableName);
        }

        public TransactionalHandler(string dbName)
        {
            if (dbName == null) throw new Exception("data base name can not be null");
            _dbName = dbName.Clone() as string;
        }

        string DbName
        {
            get { return _dbName; }
        }

        public void BeginTransaction()
        {
            Mgr.Instance.BeginTransaction(_dbName);
        }
        public void BeginTransaction(System.Data.IsolationLevel isolationLevel)
        {
            Mgr.Instance.BeginTransaction(_dbName, isolationLevel);
        }


        public void AcceptTransaction()
        {
            Mgr.Instance.AcceptTransaction(_dbName);
        }

        public void RollBackTransaction()
        {
            Mgr.Instance.RollBackTransaction(_dbName);
        }

        public DbCommand CreateNativeCommand(string storeProcedureName)
        {
            return Mgr.Instance.CreateCommand(_dbName, storeProcedureName);
        }

        public DBCommandWrapper CreateCommand(string storeProcedureName)
        {
            DBCommandWrapper result = new DBCommandWrapper();
            result.Command = Mgr.Instance.CreateCommand(_dbName, storeProcedureName);
            return result;
        }
        public DbCommand CrearSQLNAtivo(string sqlText)
        {
            return Mgr.Instance.CreateSQL(_dbName, sqlText);
        }

        public DBCommandWrapper CreateSQL(string sqlText)
        {
            DBCommandWrapper result = new DBCommandWrapper();
            result.Command = Mgr.Instance.CreateSQL(_dbName, sqlText);
            return result;

        }

        public int ExecuteStoreProcedure(CommandType commandType, string commandText)
        {
            return Mgr.Instance.ExecuteStoreProcedure(_dbName, commandType, commandText);
        }

        /// <summary>
        /// <para>Executes the <paramref name="command"/> and returns the number of rows affected.</para>
        /// </summary>
        /// <param name="command">
        /// <para>The command that contains the query to execute.</para>
        /// </param>       
        /// <seealso cref="IDbCommand.ExecuteScalar"/>
        public void ExecuteStoreProcedure(DbCommand command)
        {
            Mgr.Instance.ExecuteStoreProcedure(_dbName, command);
        }



        public void ExecuteStoreProcedure(DBCommandWrapper command)
        {
            Mgr.Instance.ExecuteStoreProcedure(_dbName, command.Command);
        }
        /// <summary>
        /// <para>Execute the <paramref name="commandText"/> interpreted as specified by the <paramref name="commandType" /> and returns an <see cref="IDataReader"></see> through which the result can be read.
        /// It is the responsibility of the caller to close the connection and reader when finished.</para>
        /// </summary>
        /// <param name="commandType">
        /// <para>One of the <see cref="CommandType"/> values.</para>
        /// </param>
        /// <param name="commandText">
        /// <para>The command text to execute.</para>
        /// </param>
        /// <returns>
        /// <para>An <see cref="IDataReader"/> object.</para>
        /// </returns>
        /// <seealso cref="IDbCommand.ExecuteReader"/>
        //		public IDataReader LeerRegistros(CommandType commandType, string commandText)
        //		{
        //			return Mgr.Instancia.LeerRegistros(_dbName, commandType, commandText);
        //		}

        /// <summary>
        /// <para>Executes the <paramref name="command"/> and returns an <see cref="IDataReader"></see> through which the result can be read.
        /// It is the responsibility of the caller to close the connection and reader when finished.</para>
        /// </summary>
        /// <param name="command">
        /// <para>The command that contains the query to execute.</para>
        /// </param>
        /// <returns>
        /// <para>An <see cref="IDataReader"/> object.</para>
        /// </returns>
        /// <seealso cref="IDbCommand.ExecuteReader"/>
        public IDataReader ReadData(DBCommandWrapper command)
        {
            return Mgr.Instance.ReadData(_dbName, command.Command);
        }

        /// <summary>
        /// <para>Executes the <paramref name="commandText"/> interpreted as specified by the <paramref name="commandType" />  and returns the first column of the first row in the resultset returned by the query. Extra columns or rows are ignored.</para>
        /// </summary>
        /// <param name="commandType">
        /// <para>One of the <see cref="CommandType"/> values.</para>
        /// </param>
        /// <param name="commandText">
        /// <para>The command text to execute.</para>
        /// </param>
        /// <returns>
        /// <para>The first column of the first row in the resultset.</para>
        /// </returns>
        /// <seealso cref="IDbCommand.ExecuteScalar"/>
        public object ExecuteScalar(CommandType commandType, string commandText)
        {
            return Mgr.Instance.ExecuteScalar(_dbName, commandType, commandText);
        }

        /// <summary>
        /// <para>Executes the <paramref name="command"/> and returns the first column of the first row in the resultset returned by the query. Extra columns or rows are ignored.</para>
        /// </summary>
        /// <param name="command">
        /// <para>The command that contains the query to execute.</para>
        /// </param>
        /// <returns>
        /// <para>The first column of the first row in the resultset.</para>
        /// </returns>
        /// <seealso cref="IDbCommand.ExecuteScalar"/>
        public object ExecuteScalar(DBCommandWrapper command)
        {
            return Mgr.Instance.ExecuteScalar(_dbName, command.Command);
        }

        public Mgr.Base GetDataBase()
        {
            return Mgr.Instance.GetDataBase(_dbName);
        }

        ////////////////////////////////////////////////////////
        public class Mgr
        {
            protected Mgr()
            {
            }

            static Mgr()
            {
                DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory()); //Probablemente sea el lugar correcto
            }
            public void BeginTransaction(string dbName)
            {
                Base b = GetDataBase(dbName);
                b.BeginTransaction();
            }

            public void AcceptTransaction(string dbName)
            {
                Base b = GetDataBase(dbName);
                b.AcceptTransaction();
            }
            public void BeginTransaction(string dbName, IsolationLevel isolationLevel)
            {
                Base b = GetDataBase(dbName);
                b.BeginTransaction(isolationLevel);
            }


            public void RollBackTransaction(string dbName)
            {
                Base b = GetDataBase(dbName);
                b.RollBackTransaction();
            }

            internal DbCommand CreateCommand(string dbName, string storeProcedureName)
            {
                Base b = GetDataBase(dbName);
                return b.Database.GetStoredProcCommand(storeProcedureName);
            }

            public DbCommand CreateSQL(string dbName, string sqlText)
            {
                Base b = GetDataBase(dbName);
                return b.Database.GetSqlStringCommand(sqlText);
            }

            public virtual int ExecuteStoreProcedure(string dbName, CommandType commandType, string commandText)
            {
                Base b = GetDataBase(dbName);
                if (b.TransactionInProgress)
                    return b.Database.ExecuteNonQuery(b.Transaction, commandType, commandText);
                else
                    return b.Database.ExecuteNonQuery(commandType, commandText);
            }

            public virtual void ExecuteStoreProcedure(string dbName, DbCommand command)
            {
                Base b = GetDataBase(dbName);
                if (b.TransactionInProgress)
                    b.Database.ExecuteNonQuery(command, b.Transaction);
                else
                    b.Database.ExecuteNonQuery(command);
            }


            public IDataReader ReadData(string dbName, DbCommand command)
            {
                Base b = GetDataBase(dbName);
                if (b.TransactionInProgress)
                    return b.Database.ExecuteReader(command, b.Transaction);
                else
                    return b.Database.ExecuteReader(command);
            }



            public object ExecuteScalar(string dbName, CommandType commandType, string commandText)
            {
                Base b = GetDataBase(dbName);
                if (b.TransactionInProgress)
                    return b.Database.ExecuteScalar(b.Transaction, commandType, commandText);
                else
                    return b.Database.ExecuteScalar(commandType, commandText);
            }

            public object ExecuteScalar(string dbName, DbCommand command)
            {
                Base b = GetDataBase(dbName);
                if (b.TransactionInProgress)
                    return b.Database.ExecuteScalar(command, b.Transaction);
                else
                    return b.Database.ExecuteScalar(command);
            }

            public Base GetDataBase(string dbName)
            {
                lock (_syncRoot)
                {
                    Base b = null;
                    if (_bases != null)
                        b = _bases[dbName] as Base;
                    if (b == null)
                    {
                        b = new Base(dbName);
                        if (_bases == null)
                            _bases = new Hashtable();
                        _bases[dbName] = b;
                    }
                    return b;
                }
            }

            static object _syncRoot = new object();


            internal static Mgr Instance
            {
                get
                {
                    lock (_syncRoot)
                    {
                        Mgr txMgr = null;

                        object obj = Thread.GetData(_txMgrSlot);

                        if (obj != null)
                            txMgr = (Mgr)obj;
                        else
                        {
                            txMgr = new Mgr();
                            Thread.SetData(_txMgrSlot, txMgr);
                            //                      
                        }

                        return txMgr;
                    }
                }
            }

            public void LoadDataSet(string dbName, DbCommand command, DataSet dataSet, string tableName)
            {
                Base b = GetDataBase(dbName);
                if (b.TransactionInProgress)
                    b.Database.LoadDataSet(command, dataSet, tableName, b.Transaction);
                else
                    b.Database.LoadDataSet(command, dataSet, tableName);
            }

            private static LocalDataStoreSlot _txMgrSlot = Thread.AllocateDataSlot();
            private IDictionary _bases;
            //
            public class Base
            {
                private Database _database;
                private DbTransaction _transaction;
                private int _txCount = 0;
                private bool _transactionRolledBack = false;
                private string _dbName;

                internal Base(string dbName)
                {
                    _dbName = dbName;
                }

                public Database Database
                {
                    get
                    {
                        if (_database == null)
                        {
                            if (_dbName.Equals(string.Empty))
                            {

                                //data base by default                             
                                _database = DatabaseFactory.CreateDatabase("MSQLServerIPYTransaccional");
                            }
                            else
                                _database = DatabaseFactory.CreateDatabase(_dbName);
                        }
                        return _database;
                    }
                }

                public DbTransaction Transaction
                {
                    get { return _transaction; }
                }

                public void BeginTransaction()
                {
                    _transactionRolledBack = false;
                    if (_txCount == 0)
                    {
                        _conection = Database.CreateConnection();
                        _conection.Open();
                        _transaction = _conection.BeginTransaction();
                    }
                    _txCount++;
                }
                public void BeginTransaction(IsolationLevel isolationLevel)
                {
                    _transactionRolledBack = false;
                    if (_txCount == 0)
                    {
                        _conection = Database.CreateConnection();
                        _conection.Open();
                        _transaction = _conection.BeginTransaction(isolationLevel);
                    }
                    else
                        throw new Exception("Se especificó isolation level en nivel > 0 de anidación");
                    _txCount++;
                }
                public void CargarDataSet(DbCommand command, DataSet dataSet, string nombreTabla)
                {
                    Mgr.Instance.LoadDataSet(_dbName, command, dataSet, nombreTabla);
                }

                public void AcceptTransaction()
                {
                    if (_transactionRolledBack) throw new Exception("Transaction Rolledback");

                    _txCount--;

                    if (_txCount == 0)
                    {
                        if (_transaction != null)
                        {
                            _transaction.Commit();
                            // con las lineas anteriores nos aseguramos
                            _transaction.Dispose();
                            // TODO: cmr.
                            // Sospecha de que aqui no se llamaba a connection.Close();
                            if (_conection != null && _conection.State != ConnectionState.Closed)
                                _conection.Close();
                        }
                    }
                }

                public void RollBackTransaction()
                {
                    if (!_transactionRolledBack && _txCount > 0)
                    {
                        if (_transaction != null)
                        {
                            _transaction.Rollback();
                            _transaction.Dispose();

                            if (_conection != null && _conection.State != ConnectionState.Closed)
                                _conection.Close();
                        }
                        _txCount = 0;
                        _transactionRolledBack = true;
                    }
                }

                public bool TransactionRolledBack
                {
                    get { return _transactionRolledBack; }
                }

                public bool TransactionInProgress
                {
                    get { return _txCount > 0; }
                }

                private DbConnection _conection;
                public DbConnection conection
                {
                    get { return _conection; }
                }
            }
        }
    }
}
