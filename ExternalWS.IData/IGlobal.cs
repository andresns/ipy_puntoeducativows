﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.IData
{
    public interface IGlobal
    {
        #region Person  
        void InsertPerson(ExternalWS.Entity.Global.Person person);
        void UpdatePerson(ExternalWS.Entity.Global.Person person);
        void DeletePerson(ExternalWS.Entity.Global.Person person);
        List<ExternalWS.Entity.Global.Person> GetPerson(ExternalWS.Entity.Global.FilterPerson filterPerson);
        List<ExternalWS.Entity.Global.Person> GetPerson();
        ExternalWS.Entity.Global.Person GetPerson(int id);
        #endregion

        #region Producto  
        void InsertProducto(ExternalWS.Entity.Global.Producto producto);
        void UpdateProducto(ExternalWS.Entity.Global.Producto producto);
        void DeleteProducto(ExternalWS.Entity.Global.Producto producto);
        List<ExternalWS.Entity.Global.Producto> GetProducto(ExternalWS.Entity.Global.FilterProducto filterProducto);
        List<ExternalWS.Entity.Global.Producto> GetProducto();
        ExternalWS.Entity.Global.Producto GetProducto(int id);
        #endregion

        #region Proveedor  
        void InsertProveedor(ExternalWS.Entity.Global.Proveedor proveedor);
        void UpdateProveedor(ExternalWS.Entity.Global.Proveedor proveedor);
        void DeleteProveedor(ExternalWS.Entity.Global.Proveedor proveedor);
        List<ExternalWS.Entity.Global.Proveedor> GetProveedor(ExternalWS.Entity.Global.FilterProveedor filterProveedor);
        List<ExternalWS.Entity.Global.Proveedor> GetProveedor();
        ExternalWS.Entity.Global.Proveedor GetProveedor(int id);
        #endregion

        #region CabeceraOrdenCompra  
        void InsertCabeceraOrdenCompra(ExternalWS.Entity.Global.CabeceraOrdenCompra cabeceraOrdenCompra);
        void UpdateCabeceraOrdenCompra(ExternalWS.Entity.Global.CabeceraOrdenCompra cabeceraOrdenCompra);
        void DeleteCabeceraOrdenCompra(ExternalWS.Entity.Global.CabeceraOrdenCompra cabeceraOrdenCompra);
        List<ExternalWS.Entity.Global.CabeceraOrdenCompra> GetCabeceraOrdenCompra(ExternalWS.Entity.Global.FilterCabeceraOrdenCompra filterCabeceraOrdenCompra);
        List<ExternalWS.Entity.Global.CabeceraOrdenCompra> GetCabeceraOrdenCompra();
        ExternalWS.Entity.Global.CabeceraOrdenCompra GetCabeceraOrdenCompra(int id);
        #endregion

        #region DetalleOrdenCompra  
        void InsertDetalleOrdenCompra(ExternalWS.Entity.Global.DetalleOrdenCompra detalleOrdenCompra);
        void UpdateDetalleOrdenCompra(ExternalWS.Entity.Global.DetalleOrdenCompra detalleOrdenCompra);
        void DeleteDetalleOrdenCompra(ExternalWS.Entity.Global.DetalleOrdenCompra detalleOrdenCompra);
        List<ExternalWS.Entity.Global.DetalleOrdenCompra> GetDetalleOrdenCompra(ExternalWS.Entity.Global.FilterDetalleOrdenCompra filterDetalleOrdenCompra);
        List<ExternalWS.Entity.Global.DetalleOrdenCompra> GetDetalleOrdenCompra();
        ExternalWS.Entity.Global.DetalleOrdenCompra GetDetalleOrdenCompra(int id);
        #endregion

        #region CabeceraCotizacion  
        void InsertCabeceraCotizacion(ExternalWS.Entity.Global.CabeceraCotizacion cabeceraCotizacion);
        void UpdateCabeceraCotizacion(ExternalWS.Entity.Global.CabeceraCotizacion cabeceraCotizacion);
        void DeleteCabeceraCotizacion(ExternalWS.Entity.Global.CabeceraCotizacion cabeceraCotizacion);
        List<ExternalWS.Entity.Global.CabeceraCotizacion> GetCabeceraCotizacion(ExternalWS.Entity.Global.FilterCabeceraCotizacion filterCabeceraCotizacion);
        List<ExternalWS.Entity.Global.CabeceraCotizacion> GetCabeceraCotizacion();
        ExternalWS.Entity.Global.CabeceraCotizacion GetCabeceraCotizacion(int id);
        #endregion

        #region DetalleCotizacion  
        void InsertDetalleCotizacion(ExternalWS.Entity.Global.DetalleCotizacion detalleCotizacion);
        void UpdateDetalleCotizacion(ExternalWS.Entity.Global.DetalleCotizacion detalleCotizacion);
        void DeleteDetalleCotizacion(ExternalWS.Entity.Global.DetalleCotizacion detalleCotizacion);
        List<ExternalWS.Entity.Global.DetalleCotizacion> GetDetalleCotizacion(ExternalWS.Entity.Global.FilterDetalleCotizacion filterDetalleCotizacion);
        List<ExternalWS.Entity.Global.DetalleCotizacion> GetDetalleCotizacion();
        ExternalWS.Entity.Global.DetalleCotizacion GetDetalleCotizacion(int id);
        #endregion
    }
}