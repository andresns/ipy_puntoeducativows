﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Business.Global
{
    public class CabeceraCotizacion
    {

        public void SaveCabeceraCotizacion(ExternalWS.Entity.Global.CabeceraCotizacion cabeceraCotizacion, int staffMemberIdCreate)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            mt.BeginTransaction();
            try
            {
                IData.IGlobal dalCabeceraCotizacion = Factory.Global.Create();

                switch (Utils.Utils.ChooseSaveAction(cabeceraCotizacion))
                {
                    case Utils.Utils.SaveActions.Create:
                        if (Utils.Utils.NeedSave(cabeceraCotizacion))
                        {
                            dalCabeceraCotizacion.InsertCabeceraCotizacion(cabeceraCotizacion);
                        }
                        break;
                    case Utils.Utils.SaveActions.Update:
                        if (Utils.Utils.NeedSave(cabeceraCotizacion))
                        {
                            dalCabeceraCotizacion.UpdateCabeceraCotizacion(cabeceraCotizacion);
                        }
                        break;

                    case Utils.Utils.SaveActions.Delete:
                        if (Utils.Utils.NeedSave(cabeceraCotizacion))
                        {
                            dalCabeceraCotizacion.DeleteCabeceraCotizacion(cabeceraCotizacion);
                        }
                        break;
                    default:
                        break;


                }
                mt.AcceptTransaction();
            }
            catch (Exception e)
            {
                mt.RollBackTransaction();
                throw e;
            }
        }

        public List<ExternalWS.Entity.Global.CabeceraCotizacion> GetCabeceraCotizacion(ExternalWS.Entity.Global.FilterCabeceraCotizacion filterCabeceraCotizacion)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalCabeceraCotizacion = Factory.Global.Create();
            List<ExternalWS.Entity.Global.CabeceraCotizacion> cabeceraCotizacionList = dalCabeceraCotizacion.GetCabeceraCotizacion(filterCabeceraCotizacion);
            return cabeceraCotizacionList;
        }

        public List<ExternalWS.Entity.Global.CabeceraCotizacion> GetCabeceraCotizacion()
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalCabeceraCotizacion = Factory.Global.Create();
            List<ExternalWS.Entity.Global.CabeceraCotizacion> cabeceraCotizacionList = dalCabeceraCotizacion.GetCabeceraCotizacion();
            return cabeceraCotizacionList;
        }

        public ExternalWS.Entity.Global.CabeceraCotizacion GetCabeceraCotizacion(int id)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalCabeceraCotizacion = Factory.Global.Create();
            ExternalWS.Entity.Global.CabeceraCotizacion cabeceraCotizacionList = dalCabeceraCotizacion.GetCabeceraCotizacion(id);
            return cabeceraCotizacionList;
        }
    }
}