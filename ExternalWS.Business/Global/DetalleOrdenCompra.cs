﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Business.Global
{
    public class DetalleOrdenCompra
    {

        public void SaveDetalleOrdenCompra(ExternalWS.Entity.Global.DetalleOrdenCompra detalleOrdenCompra, int staffMemberIdCreate)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            mt.BeginTransaction();
            try
            {
                IData.IGlobal dalDetalleOrdenCompra = Factory.Global.Create();

                switch (Utils.Utils.ChooseSaveAction(detalleOrdenCompra))
                {
                    case Utils.Utils.SaveActions.Create:
                        if (Utils.Utils.NeedSave(detalleOrdenCompra))
                        {
                            dalDetalleOrdenCompra.InsertDetalleOrdenCompra(detalleOrdenCompra);
                        }
                        break;
                    case Utils.Utils.SaveActions.Update:
                        if (Utils.Utils.NeedSave(detalleOrdenCompra))
                        {
                            dalDetalleOrdenCompra.UpdateDetalleOrdenCompra(detalleOrdenCompra);
                        }
                        break;

                    case Utils.Utils.SaveActions.Delete:
                        if (Utils.Utils.NeedSave(detalleOrdenCompra))
                        {
                            dalDetalleOrdenCompra.DeleteDetalleOrdenCompra(detalleOrdenCompra);
                        }
                        break;
                    default:
                        break;


                }
                mt.AcceptTransaction();
            }
            catch (Exception e)
            {
                mt.RollBackTransaction();
                throw e;
            }
        }

        public List<ExternalWS.Entity.Global.DetalleOrdenCompra> GetDetalleOrdenCompra(ExternalWS.Entity.Global.FilterDetalleOrdenCompra filterDetalleOrdenCompra)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalDetalleOrdenCompra = Factory.Global.Create();
            List<ExternalWS.Entity.Global.DetalleOrdenCompra> detalleOrdenCompraList = dalDetalleOrdenCompra.GetDetalleOrdenCompra(filterDetalleOrdenCompra);
            return detalleOrdenCompraList;
        }

        public List<ExternalWS.Entity.Global.DetalleOrdenCompra> GetDetalleOrdenCompra()
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalDetalleOrdenCompra = Factory.Global.Create();
            List<ExternalWS.Entity.Global.DetalleOrdenCompra> detalleOrdenCompraList = dalDetalleOrdenCompra.GetDetalleOrdenCompra();
            return detalleOrdenCompraList;
        }

        public ExternalWS.Entity.Global.DetalleOrdenCompra GetDetalleOrdenCompra(int id)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalDetalleOrdenCompra = Factory.Global.Create();
            ExternalWS.Entity.Global.DetalleOrdenCompra detalleOrdenCompraList = dalDetalleOrdenCompra.GetDetalleOrdenCompra(id);
            return detalleOrdenCompraList;
        }
    }
}