﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.DataContracts
{
    [DataContract]
    public class DetalleCotizacion
    {
        int _cantidad = 0;
        int _precioUnitario = 0;
        int _idProducto = 0;
        string _codCotizacion = "";

        [DataMember]
        public int Cantidad
        {
            get { return _cantidad; }
            set { _cantidad = value; }
        }

        [DataMember]
        public int PrecioUnitario
        {
            get { return _precioUnitario; }
            set { _precioUnitario = value; }
        }

        [DataMember]
        public int IdProducto
        {
            get { return _idProducto; }
            set { _idProducto = value; }
        }

        [DataMember]
        public string CodCotizacion
        {
            get { return _codCotizacion; }
            set { _codCotizacion = value; }
        }
    }

    [DataContract]
    public class FilterDetalleCotizacion
    {
        int _cantidad = 0;
        int _precioUnitario = 0;
        int _idProducto = 0;
        string _codCotizacion = "";

        [DataMember]
        public int Cantidad
        {
            get { return _cantidad; }
            set { _cantidad = value; }
        }

        [DataMember]
        public int PrecioUnitario
        {
            get { return _precioUnitario; }
            set { _precioUnitario = value; }
        }

        [DataMember]
        public int IdProducto
        {
            get { return _idProducto; }
            set { _idProducto = value; }
        }

        [DataMember]
        public string CodCotizacion
        {
            get { return _codCotizacion; }
            set { _codCotizacion = value; }
        }
    }
}