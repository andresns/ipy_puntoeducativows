﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.DataContracts
{
    [DataContract]
    public class CabeceraCotizacion
    {
        string _codCotizacion = "";
        DateTime _fecha;
        string _runProveedor = "";

        [DataMember]
        public string CodCotizacion
        {
            get { return _codCotizacion; }
            set { _codCotizacion = value; }
        }

        [DataMember]
        public DateTime Fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }

        [DataMember]
        public string RunProveedor
        {
            get { return _runProveedor; }
            set { _runProveedor = value; }
        }
    }

    [DataContract]
    public class FilterCabeceraCotizacion
    {
        string _codCotizacion = "";
        DateTime _fecha;
        string _runProveedor = "";

        [DataMember]
        public string CodCotizacion
        {
            get { return _codCotizacion; }
            set { _codCotizacion = value; }
        }

        [DataMember]
        public DateTime Fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }

        [DataMember]
        public string RunProveedor
        {
            get { return _runProveedor; }
            set { _runProveedor = value; }
        }
    }
}
