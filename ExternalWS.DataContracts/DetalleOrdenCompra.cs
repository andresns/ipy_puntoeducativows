﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.DataContracts
{
    [DataContract]
    public class DetalleOrdenCompra
    {
        int _cantidad = 0;
        int _precioUnitario = 0;
        int _idProducto = 0;
        string _codOrdenCompra = "";

        [DataMember]
        public int Cantidad
        {
            get { return _cantidad; }
            set { _cantidad = value; }
        }

        [DataMember]
        public int PrecioUnitario
        {
            get { return _precioUnitario; }
            set { _precioUnitario = value; }
        }

        [DataMember]
        public int IdProducto
        {
            get { return _idProducto; }
            set { _idProducto = value; }
        }

        [DataMember]
        public string CodOrdenCompra
        {
            get { return _codOrdenCompra; }
            set { _codOrdenCompra = value; }
        }
    }

    [DataContract]
    public class FilterDetalleOrdenCompra
    {
        int _cantidad = 0;
        int _precioUnitario = 0;
        int _idProducto = 0;
        string _codOrdenCompra = "";

        [DataMember]
        public int Cantidad
        {
            get { return _cantidad; }
            set { _cantidad = value; }
        }

        [DataMember]
        public int PrecioUnitario
        {
            get { return _precioUnitario; }
            set { _precioUnitario = value; }
        }

        [DataMember]
        public int IdProducto
        {
            get { return _idProducto; }
            set { _idProducto = value; }
        }

        [DataMember]
        public string CodOrdenCompra
        {
            get { return _codOrdenCompra; }
            set { _codOrdenCompra = value; }
        }
    }
}
