﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.DataContracts
{
    [DataContract]
    public class Producto
    {
        int _idProducto = 0;
        string _descripcion = "";

        [DataMember]
        public int IdProducto
        {
            get { return _idProducto; }
            set { _idProducto = value; }
        }

        [DataMember]
        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }
        

    }

    [DataContract]
    public class FilterProducto
    {
        int _idProducto = 0;
        string _descripcion = "";
        string _systemName = "";
        string _password = "";

        [DataMember]
        public int IdProducto
        {
            get { return _idProducto; }
            set { _idProducto = value; }
        }

        [DataMember]
        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }

        [DataMember]
        public string SystemName
        {
            get { return _systemName; }
            set { _systemName = value; }
        }

        [DataMember]
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
    }
}
